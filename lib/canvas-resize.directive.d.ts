import { AfterViewInit, Renderer2, ElementRef } from '@angular/core';
export declare class CanvasResizeDirective implements AfterViewInit {
    private canvasElementRef;
    private renderer;
    private signaturePad;
    private debounceTime?;
    constructor();
    ngAfterViewInit(): void;
    private resizeCanvas;
}
